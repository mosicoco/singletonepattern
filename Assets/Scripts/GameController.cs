using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace SingletonPattern
{


    public class GameController : MonoBehaviour
    {
        public void TestCharpSingleton()
        {
            Debug.Log("C#");
            SingletoneCSharp instance = SingletoneCSharp.Instance;
            instance.TestSingleton();
            SingletoneCSharp instance2 = SingletoneCSharp.Instance;
            instance2.TestSingleton();
        }
        public void TestUnitySingleton()
        {
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleton();
        }

    }
}