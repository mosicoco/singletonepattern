using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingletoneCSharp
    {
        private static SingletoneCSharp instance = null;

        public static SingletoneCSharp Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingletoneCSharp();
                }
                return instance;
            }
        }
        private float randomNumber;
        private SingletoneCSharp()
        {
            randomNumber = Random.Range(0f, 1f);
        }
        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is: {randomNumber}");
        }
        
    }
}
